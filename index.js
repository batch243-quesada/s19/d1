// console.log('Hello, World');

// Conditional Statements

// if-else statement

// ternary operator
// 1. condition
// 2. expression to execute if condition is truthy
// 3. expression if the condition is falsy
// Ternary operators have an implciit "return" statement meaning without return keyword, the resulting expressions can be stored in a variable

// let ternaryResult = ( 27 < 18 ) ? 1 : 2;
// console.log(`Result of ternary operator is ${ternaryResult}.`);

// function isOfLegalAge() {
// 	name = prompt('What is your name?');
// 	return 'You are of legal age';
// }

// function isUnderAge () {
// 	name = prompt('What is your name?');
// 	return 'You are under age limit';
// }

// let name;

// // The parseInt() function converts the input received into a number data type
// let age = parseInt(prompt("What is your age?"));
// console.log(age);	

// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
// console.log(`Result of Ternary Operator in Functions: ${legalAge}, ${name}.`);


// try-catch-finally statement
// commonly used for error handling
// there are instances when the application returns an error/warning that is not necessarily an error in the context of our code
// these errors are result of an attempt of the programming language to help developers in creating efficient code
// they are used to specify a response whenever an exception/erroris received

function showIntensityAlert(windSpeed) {
	try {
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error) {
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show alert.")
	}
}

showIntensityAlert(110);